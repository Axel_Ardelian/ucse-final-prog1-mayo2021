﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Programa
    {
        public int CodigoDelPrograma { get; set; }
        public string NombreDelPrograma { get; set; }
        public DateTime FechaDeInicioDeEmision { get; set; }
        public string Categoria { get; set; }
        public enum Tipo
        {
            Lunes,
            Martes,
            Miercoles,
            Jueves,
            Viernes
        }
        public Tipo DiaDeLaSemana { get; set; }
        public DateTime HorarioDeInicio { get; set; }
        public DateTime HorarioDeFinalizacion { get; set; }

        public abstract string Devolver(Programa programa);
    }
}
