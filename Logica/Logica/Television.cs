﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Television : Programa
    {
        public string NombreConductor1 { get; set; }
        public string NombreConductor2 { get; set; }
        public string Canal { get; set; }

        public override string Devolver(Programa programa)
        {
            return programa.NombreDelPrograma + "(Conductores: " + NombreConductor1 + NombreConductor2 + ")" + programa.FechaDeInicioDeEmision + "desde las " + HorarioDeInicio + "hasta las" + HorarioDeFinalizacion + "por el canal: " + Canal;
        }
    }
}
