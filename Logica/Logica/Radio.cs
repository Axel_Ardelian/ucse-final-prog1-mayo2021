﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Radio : Programa
    {
        public string NombreConductorPrincipal { get; set; }
        public string Frecuencia { get; set; }

        public override string Devolver(Programa programa)
        {
            return "Tu radio favorita conducida por " + NombreConductorPrincipal + "siempre en la misma frecuencia: " + Frecuencia;
        }
    }
}
