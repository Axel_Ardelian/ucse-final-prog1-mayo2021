﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Internet : Programa
    {
        public enum TipoDePlat
        {
            Instagram,
            Facebook,
            Youtube
        }
        public TipoDePlat Plataforma { get; set; }
        public Tipo TipodeDia { get; set; }
        public string Titulo { get; set; }
        public string NombreDelManager { get; set; }

        public override string Devolver(Programa programa)
        {
            return "No te pierdas en: " + Plataforma + " el streaming " + Titulo + "llevado adelante por " + NombreDelManager;
        }
    }
}
