﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Sorteo
    {
        public int CodigoDelSorto { get; set; }
        public int CodigoDelPrograma { get; set; }
        public int DNI { get; set; }
        public string DescripcionDelPremio { get; set; }
        public List<Inscripto> InscriptosEnElSorteo { get; set; }

    }
}
