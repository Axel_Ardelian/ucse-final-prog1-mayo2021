﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        List<Programa> Programas = new List<Programa>();
        List<Sorteo> Sorteos = new List<Sorteo>();
        List<Inscripto> Inscriptos = new List<Inscripto>();

        public void InscribirPersona(Inscripto inscripto)
        {
            Sorteo NuevoSorteo = new Sorteo();
            NuevoSorteo.CodigoDelSorto = CrearCodigoSorteo();
            NuevoSorteo.CodigoDelPrograma = CrearCodigoPrograma();
            NuevoSorteo.DNI = inscripto.DNI;
            NuevoSorteo.InscriptosEnElSorteo.Add(inscripto);
            Sorteos.Add(NuevoSorteo);
        }
        public int CrearCodigoSorteo()
        {
            int numero = Sorteos.Count();
            return numero += 1;           
        }
        public int CrearCodigoPrograma()
        {
            int numero = Programas.Count();
            return numero += 1;
        }
        public List<string> DevolverDescripciones(DateTime dia1, int tipoDePlataforma)
        {
            List<string> Descripciones = new List<string>();
            Programa programa = Programas.Find(x => x.FechaDeInicioDeEmision == dia1);            
            if (programa != null)
            {
                programa = Programas.Find(x => x.CodigoDelPrograma == tipoDePlataforma);
                if (programa != null)
                {
                    Descripciones.Add(programa.Categoria);
                }
                Descripciones.OrderBy(x=> programa.FechaDeInicioDeEmision);
            }
            return Descripciones;
        }
        public List<Programa> ElaborarListado(Programa programa)
        {
            List<Programa> Radiales = new List<Programa>();
            if (Convert.ToInt32(programa.DiaDeLaSemana) == 1)
            {              
                foreach (var item in Programas)
                {
                    Radiales.Add(programa);
                }
            }
            return Radiales;
        }
        public Ganador SortearPremio()
        {
            Ganador ganador = new Ganador();
            Random random = new Random();
            int numeroSorteado = random.Next(0, Sorteos.Count);
            Sorteo Validacion = Sorteos.Find(x => x.CodigoDelSorto == numeroSorteado);          
            if (Validacion == null)
            {                
                ganador.DNI = Validacion.DNI;
                ganador.Mensaje = "El DNI ganador es: " + ganador.DNI; 
            }
            else
            {
                ganador.Mensaje = "El sorteo ya tiene un ganador.";
            }
            return ganador;
           
        }
    }
}
